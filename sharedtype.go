package sharedtypes

import (
	nodderlib "bitbucket.org/n0needt0/nodderlib"
	"bitbucket.org/n0needt0/uppr"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	DB_CRONRAT_RAT  = "cronrat_rat" //main database
	COL_CRONRAT_RAT = "cronrat_rat" //this where rats live

	DB_CRONRAT_TOKENS  = "cronrat_tokens"
	COL_CRONRAT_TOKENS = "cronrat_tokens" //all permitted tokens

	DB_SNOOPY_SCANS  = "snoopy_scans"
	COL_SNOOPY_SCANS = "snoopy_scans" //this is where scan records go to

	DB_SNOOPY_HIST  = "snoopy_hist"
	COL_SNOOPY_HIST = "snoopy_hist" //this is where scan records go to

	DB_SNOOPY_MACS  = "snoopy_macs"
	COL_SNOOPY_MACS = "snoopy_macs" //all vetted mac addresses go here

	DB_SNOOPY_TOKENS  = "snoopy_tokens"
	COL_SNOOPY_TOKENS = "snoopy_tokens" //all permitted tokens

	DB_NODDER_NODES  = "nodder_nodes"
	COL_NODDER_NODES = "nodder_nodes"

	DB_NODDER_JOBS  = "nodder_jobs"
	COL_NODDER_JOBS = "nodder_jobs"

	DB_NODDER_HISTORY  = "nodder_history"
	COL_NODDER_HISTORY = "nodder_history"

	DB_NODDER_EVENTS  = "nodder_events"
	COL_NODDER_EVENTS = "nodder_events"

	DB_HAWLER_ALERTS  = "hawler_alerts"
	COL_HAWLER_ALERTS = "hawler_alerts"

	DB_SYS_LOCKS  = "sys_locks"
	COL_SYS_LOCKS = "sys_locks"

	DB_USERS  = "users"
	COL_USERS = "users"

	DEFAULT_CRON_1MIN = "0 */1 * * * * *"

	SEC_30   = 30
	HR24_SEC = 24 * 60 * 60

	SYSTEM_USER = "0"
)

// lock is used to create a queue
//by using mongos FindAndModify
type JobLock struct {
	Chkid      string `json:"chkid", bson:"chkid"`
	Nodeid     string `json:"nodeid", bson:"nodeid"`
	CrontabAdd int    `json:"crontabadd", bson:"crontabadd"`
	Bts        int64  `json:"bts", bson:"bts"`
	Lr         int64  `json:"lr", bson:"lr"`
	Nr         int64  `json:"nr", bson:"nr"`
}

//thisis list of jobs
type JobLine struct {
	Chkid    string        `json:"chkid", bson:"chkid"`
	Userid   string        `json:"userid", bson:"userid"`
	Crontab  string        `json:"crontab", bson:"crontab"`
	JitterUp int           `json:"jup", bson:"jup"`
	JitterDn int           `json:"jdn", bson:"jdn"`
	Weight   int           `json:"weight", bson:"weight"`
	Job      nodderlib.Job `json:"job", bson:"job"`
}

//this is JobHistory
type JobHistory struct {
	Chkid     string        `json:"chkid", bson:"chkid"`
	Nodeid    string        `json:"nodeid", bson:"nodeid"`
	Job       nodderlib.Job `json:"job", bson:"job"`
	CreatedAt time.Time     `json:"createdAt" bson:"createdAt"`
}

//this is JobEvent
type JobEvent struct {
	Chkid     string        `json:"chkid", bson:"chkid"`
	Nodeid    string        `json:"nodeid", bson:"nodeid"`
	Job       nodderlib.Job `json:"job", bson:"job"`
	Ack       string        `json:"ack", bson:"ack"`
	CreatedAt time.Time     `json:"createdAt" bson:"createdAt"`
}

//used in snoopy-home and cronrat
type Rat struct {
	Token       string `json:"token" bson:"token"`
	Name        string `json:"name" bson:"name"`
	Email       string `json:"email" bson:"email"`
	RatSpec     string `json:"rats" bson:"rats"`
	NextRatTs   int64  `json:"nextratts" bson:"nextratts"`
	State       int64  `json:"state" bson:"state"`
	AlertCnt    int    `json:"alertcnt" bson:"alertcnt"`
	LastAlertTs int64  `json:"lastalertts" bson:"lastalertts"`
	Updated     int64  `json:"updated" bson:"updated"`
}

//used in snoopy/snoopy-home
type ScanReport struct {
	ID     bson.ObjectId       `bson:"_id,omitempty"`
	Token  string              `json:"token", bson:"token"`
	Alert  bool                `json:"alert", bson:"alert"`
	Start  int64               `json:"start", bson:"start"`
	Finish int64               `json:"finish", bson:"finish"`
	FromIp string              `json:"fromip", bson:"fromip"`
	Diff   map[string][]string `json:"diff", bson:"diff"`
	Scan   map[string][]string `json:"scan", bson:"scan"`
}

func LockMaster(M *mgo.Session, service string, nodeid string, timetowaitsec int) bool {

	mongo := M.Copy()
	defer mongo.Close()
	mongo.SetMode(mgo.Strong, true)
	mongo.Refresh()

	var log = uppr.GetLog()

	c := mongo.DB(DB_SYS_LOCKS).C(COL_SYS_LOCKS)

	lock := struct {
		Service   string `bson:"service"`
		LockedTo  string `bson:"lockedto"`
		LastRunTs int64  `bson:"lastrun"`
	}{}

	//try to insert empty lock if it does not exists
	err := c.Insert(bson.M{"service": service, "lastrun": 0, "lockedto": "start"})
	if err != nil {
		log.Debugf("lock exists for %s", service)
	}

	log.Debugf("Trying to lock %s with %s", service, nodeid)

	//find lock
	query := bson.M{"service": service, "lastrun": bson.M{"$lt": time.Now().Unix() - int64(timetowaitsec)}}

	//this is info we will try to lock with
	change := mgo.Change{
		Update:    bson.M{"$set": bson.M{"lastrun": time.Now().Unix(), "lockedto": nodeid}},
		ReturnNew: false,
	}

	info, err := c.Find(query).Apply(change, &lock)
	if err != nil {
		log.Debugf("Query result %v, %s, %v", info, err, query)
	}

	//find lock
	query = bson.M{"service": service}

	err = c.Find(query).One(&lock)
	if err != nil {
		log.Debugf("Query result	%s", err.Error())
		return false
	}

	if lock.LockedTo != nodeid {
		//we are good to go
		log.Debugf("already locked to %s", lock.LockedTo)
		return true
	} else {
		//we are good to go
		log.Debugf("locked to %s", lock.LockedTo)
		return true
	}
}
